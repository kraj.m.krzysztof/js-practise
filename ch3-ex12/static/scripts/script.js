import Shelf from './shelf_obj.js';
import Book from './book_obj.js';
import {Header, Footer} from './layout.js';
import { AddBookForm } from './forms.js';

const header = new Header('');
header.append();

const footer = new Footer('');
footer.append();

const shelf = new Shelf(
    'Book shelf',
    40,
    30,
    80,
    5
)

const cleanCode = new Book(
    'Clean code: A Handbook of Agile Software Craftmenship',
    'Robert Cecil Martin',
    464,
    2008,
    'Soft'
)

const drive = new Book(
    'Drive: The Suprising Truth About What Motivates Us',
    'Daniel H. Pink',
    256,
    2009,
    'Soft'
)

drive.read(500);

shelf.addBook(cleanCode);
shelf.addBook(drive);
let shelfBooks = shelf.convertToHtmlElement();
let myBooks = document.getElementById('my_books');
myBooks.append(shelfBooks);

let addBookForm = new AddBookForm()
addBookForm.renderAndAppend('main--book_form')
// document.getElementById('my_books').innerHTML = shelf.convertToHtml();