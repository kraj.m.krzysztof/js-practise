export class AddBookForm {
  constructor(
  ){
  }
  renderAndAppend(parentNodeClass){
    const formParentNode = document.querySelector('.' + parentNodeClass);
    formParentNode.append(this.render());
  }
  render() {
    const addBookForm = document.createElement('form');
    addBookForm.setAttribute('id', 'add_book_form')

    let bookName = new BookFormField("Name:", "bname", "text", "");
    addBookForm.append(bookName.getCompleteFieldContainer());

    let bookAuthor = new BookFormField("Author:", "bauthor", "text", "");
    addBookForm.append(bookAuthor.getCompleteFieldContainer());

    let bookPagesCount = new BookFormField("Pages count:", "bpagesCount", "number", "");
    addBookForm.append(bookPagesCount.getCompleteFieldContainer());

    let bookReleaseYear = new BookFormField("Release year:", "byear", "number", "");
    addBookForm.append(bookReleaseYear.getCompleteFieldContainer());

    let bookCoverType = new BookFormField("Hard cover:", "bcover", "radio", "");
    addBookForm.append(bookCoverType.getCompleteFieldContainer());

    let bookSubmit = new BookFormField("", "bsubmit", "submit", "Add book");
    addBookForm.append(bookSubmit.getCompleteFieldContainer());
    
    return addBookForm;
  }
}


class BookFormField {
  constructor(
    description,
    id,
    type,
    value
  ) {
    this.description = description,
    this.id = id,
    this.type = type,
    this.value = value
  }
  getFieldElement() {
    if (this.isSubmitType()) {
      return this.createInput();
    }
    return this.getCompleteFieldContainer();
  };
  getCompleteFieldContainer(){
    let container = this.createContainer()
    container.appendChild(this.createLabel());
    container.appendChild(this.createInput());
    return container;
  };
  createContainer() {
    let fieldContainer = document.createElement('div');
    fieldContainer.classList.add('book_form_element');
    return fieldContainer;
  };
  createLabel() {
    let labelField = document.createElement('label');
    labelField.textContent = this.description;
    labelField.setAttribute('for', this.id);
    return labelField;
  };
  createInput() {
    let inputField = document.createElement('input');
    inputField.setAttribute('type', this.type);
    inputField.setAttribute('id', this.id);
    if (this.isSubmitType()) {
      this.addSubmitAttributes(inputField);
    }
    return inputField;
  };
  addSubmitAttributes(htmlElement){
    htmlElement.setAttribute('value', this.value);
    htmlElement.setAttribute('class', 'button button__form');
  };
  isSubmitType(){
    return (this.type === 'submit');
  };
}