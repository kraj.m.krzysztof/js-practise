let areReadMarked = false;
let isFormVisible = false;

function colorReadBooks() {
    const readBooks = document.querySelectorAll(".read");
    if (readBooks.length === 0) {
        window.alert('You have not finished any of the books yet!');
        return;
    }
    readBooks.forEach((book) => {
        if (!areReadMarked) {
            book.style.backgroundColor = "var(--read_book_container_bg)";
        } else {
            book.style.backgroundColor = "var(--book_container_bg)";
        }
    })
    areReadMarked = !(areReadMarked);
    setFinishedBooksBtnCaption();
}
function setFinishedBooksBtnCaption(){
    let actionBtn = document.getElementById('btn_mark_read');
    let btnCaption = ((areReadMarked) ? "Unmark finished books": "Mark finished books");
    actionBtn.textContent = btnCaption;
}

var bookForm = document.querySelector(".main--book_form");
var formVisibilityChangeBtn = document.querySelector(".collapsible");
formVisibilityChangeBtn.addEventListener("click", function() {
    bookForm.style.display = ((isFormVisible) ? "none" : "flex");
    setVisibilityBtnCaption();
    isFormVisible = (!isFormVisible);
})
function setVisibilityBtnCaption(){
    formVisibilityChangeBtn.textContent = ((isFormVisible) ? "Add a book to your shelf" : "Hide form");
}
function boldBooksNames() {
    const books = document.querySelectorAll(".book_name");
    books.forEach((book) => {
        ((hasBoldStyle(book)) ? unsetBoldStyle(book) : setBoldStyle(book));
    })
}
function hasBoldStyle(element){
    return (element.style.fontWeight === 'bold');
}
function setBoldStyle(element){
    element.style.fontWeight = "bold";
}
function unsetBoldStyle(element){
    element.style.fontWeight = "";
}