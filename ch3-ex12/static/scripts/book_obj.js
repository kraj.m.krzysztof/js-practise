class Book {
  constructor (
      // Defines parameters:
      name,
      author,
      pagesCount,
      releaseYear,
      cover
  ) {
      // Define parameters
      this.name = name,
      this.author = author,
      this.pagesCount = pagesCount,
      this.releaseYear = releaseYear,
      this.cover = cover,
      this.pagesRead = 0,
      this.isRead = false
  }
  read(numOfPages) {
    if (isNaN(numOfPages)) {
      console.log('Cannot read' + numOfPages + 'as it is not a number.');
    } else if (numOfPages < 0){
      console.log('Cannot unread pages.');
    } else if ((this.pagesRead + numOfPages) >= this.pagesCount) {
      this.isRead = true;
      this.pagesRead = this.pagesCount;
      console.log('You have read ' + this.name + '!');
    } else {
      this.pagesRead += numOfPages;
    }
    return;
  };
  convertToHtmlElement(){
    let bookElement = document.createElement('div');
    bookElement.classList.add('shelf_book');
    let readingStatus =  ((this.isRead) ? 'read' : 'not_read');
    bookElement.classList.add(readingStatus);
    bookElement.innerHTML = this.convertToHtmlParagraphs();
    return bookElement;
  }
  convertToHtmlParagraphs(){
    return `
    <p>Book: <span class="book_name">${this.name}</span> by ${this.author}</p>
    <p>Reading progress: <span class="book_reading_progress">${this.pagesRead}/${this.pagesCount}</span></p>
    <div class="book_other_info">
      <p>Relase year: ${this.releaseYear}</p>
      <p>Cover type: ${this.cover}</p>
    <div>
    `
  };
  convertToHtmlRow(){
    return `
    <tr>
      <td>${this.name}</td>
      <td>${this.author}</td>
      <td>${this.pagesCount}</td>
      <td>${this.releaseYear}</td>
      <td>${this.cover}</td>
    </tr>
    `
  };
}

export default Book;