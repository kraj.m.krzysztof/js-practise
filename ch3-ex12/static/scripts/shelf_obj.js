class Shelf {
    constructor (
        // Define parameters
        name,
        height,
        depth,
        width,
        maxCapacity,
    ) {
        // Assign parameters
        this.name = name,
        this.dimensions = {
          height: height,
          depth: depth,
          width: width,
        }
        this.maxCapacity = maxCapacity,
        this.books = []
    }
    isEmpty() {
      return (this.books.length === 0);
    }
    isFull() {
      return (this.books.length === this.maxCapacity);
    }
    addBook(book) {
      if (this.isFull()) {
        console.log('Cannot add any more books, shelf at it\'s capacity!');
        return;
      }
      this.books.push(book);
      console.log(`Sucessfully added ${book.name}.`);
    };
    removeBook(book) {
      if (this.isEmpty()) {
        console.log('Cannot remove a book from empty shelf');
        return;
      }
      let bookIndex = this.books.indexOf(book);
      if (bookIndex === -1) {
        console.log(`No book named ${book.name} on the shelf.`);
        return;
      }
      this.books.splice(bookIndex, 1);
      console.log(`Removed ${book.name} from the shelf.`);
    };
    convertToHtmlElement() {
      let shelfBooks =  document.createElement('div');
      shelfBooks.classList.add('shelf_books')
      if (this.isEmpty()) {
        shelfBooks.append(this.createNoBooksInformation());
      } else {
        this.books.forEach((book) => {
          shelfBooks.append(book.convertToHtmlElement());
        })
      }
      return shelfBooks;
    };
    createNoBooksInformation() {
      let noBooks = document.createElement('p');
      noBooks.textContent = 'You have no books on your shelf!';
      return noBooks;
    }
}

export default Shelf;