export class Header {
  constructor(
  ) {
  }
  append() {
    const pageHeader = document.querySelector('.header');
    pageHeader.innerHTML = this.render();
  }
  render(){
    return `
    <h1>My book shelf</h1>
    <div>
      <h2>All my books in one place</h2>
      <h3>Add new, read them and remove uneccessary ones</h3>
    </div>
    `
  }
}

export class Footer {
    constructor(
    ) {
    }
    append() {
      const pageFooter = document.querySelector('.footer');
      pageFooter.innerHTML = this.render();
    }
    render() {
      let linkedInLink = new Link(
        "My LinkedIn profile:",
        "https://www.linkedin.com/in/krzysztof-kraj/",
        "Krzysztof Kraj at Linkedin"
      )
      let gitLabLink = new Link(
        "My GitLab:",
        "https://gitlab.com/kraj.m.krzysztof",
        "Krzysztof Kraj at GitLab"
      )
      return `
        <p>Designed and developed purely for education purposes</p>
        <div class="footer_links">
          ${linkedInLink.render()}
          ${gitLabLink.render()}
        </div>
      `
    }
}

class Link {
  constructor(
    description,
    href,
    hrefOverwrite
  ) {
    this.description = description,
    this.href = href,
    this.hrefOverwrite = hrefOverwrite
  }
  render() {
    return `
    <div class="link_container">
      <p class="link_description">${this.description}</p>
      <a class="link" href="${this.href}">${this.hrefOverwrite}</a>
    </div>
    `
  }
}