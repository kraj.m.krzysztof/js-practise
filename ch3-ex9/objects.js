const shelf = {
    name: "Book shelf",
    dimensions: {
        height: 50,
        depth: 30,
        width: 70,
    },
    books: [],
    maxCapacity: 5,
    isEmpty: true,
    isFull: false,
    addBook: function(bookName) {
        if (!this.isFull){
            this.books.push(bookName);
            console.log("Added", bookName, "to the shelf.");
            this.checkIfEmpty();
            this.checkIfFull();
            return;
        }
        console.log("Shelf already full, failed to add", bookName);
    },
    checkIfEmpty: function() {
        this.isEmpty = (this.books.length === 0);
    },
    checkIfFull: function() {
        this.isFull = (this.books.length === this.maxCapacity);
        return ((this.isFull) ? "Shelf is at it's capacity." : "You can still store more books");
    },
    removeBook: function(bookName){
        if (this.isEmpty){
            console.log("Shelf is already empty, cannot remove", bookName);
            return
        }
        let bookIndex = this.books.indexOf(bookName);
        if (bookIndex > -1) {
            this.books.splice(bookIndex, 1);
            console.log("Successfully removed", bookName, "from the shelf.");
            this.checkIfEmpty();
            return
        }
        console.log(bookName, "is not on the shelf, cannot remove it.");
        return
    }
}